# GPS

## Installation overview

```bash
git clone https://gitlab.com/ofp-oc-cda2/adrien/gps.git
cp .env.template .env
```

you can rewrite .enf file. after, run:

```bash
npm install
docker-compose up
```

## command
launch test: `npm test`
suivre les logs d'un service en particulier: `sudo docker-compose logs -f nameOfService`

## example .env

DATABASE_URL=mongodb://mongo:27017/gps
DATABASE_authSource=admin
DATABASE_user=root
DATABASE_pass=lePassDeMongo

## Check list of development

### done

 - [x] function in rootChamp.js > CHAMP DELETE
 - [x] put .env in docker-compose.yml (and change pass)
 - [x] move mongo model (User) in a schema folder
 - [x] move the roads from index.js (racine) to controllers folder
 - [x] see the problem: duplication fields (champs) with the same _id when using USER > CHAMP SINGLE > UPDATE 
 - [x] see the problem: duplication fields (champs) with the same _id when using calendar > update
 - [x] jwt: prohibit operations on other users ( invalid token )
 - [x] update user change password (bcrypt, ...)
 - [x] create jwt operation on roads:
    - [x] USER UPDATE
    - [x] USER DELETE
    - [x] USER READ ONE
    - [x] USER > CHAMP SINGLE > ADD
    - [x] USER > CHAMP SINGLE > READ
    - [x] USER > CHAMP SINGLE > REMOVE
    - [x] USER > CHAMP SINGLE > UPDATE
    - [x] USER > CHAMP SINGLE > CALENDAR > ADD
    - [x] USER > CHAMP SINGLE > CALENDAR > READ (FOR CHAMP)
    - [x] USER > CHAMP SINGLE > CALENDAR > DELETE
    - [x] USER > CHAMP SINGLE > CALENDAR > UPDATE
 - [x] create fonction for generate new pass with send mail => integrated with user update?
 - [x] factorized the 2 functions (merge in one) for generate hash (create user and update password in rootUser.js).
 - [x] userReadOne must return calendars in fields (champs).
 - [x] see view graphic per week (logique pour chevauchement des années) => on client (vue).

### to do

 - [ ] see send mail for CREATE ACCOUNT with real certified mail.
 - [ ] see send mail "I forgot my password".
