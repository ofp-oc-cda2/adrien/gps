const tagOfTest = Math.round(Math.random() * 1000);
const pass = '0000';

const assert = require('assert').strict;
// const { request } = require('http');
const request = require('request');

const express = require('express');
const cors = require('cors');

const app = express();
app.use(cors());
app.use(express.json());

const fetchUrl = 'http://localhost:8080';

// const chai = require('chai'); => est install

const {
    mySendMailFunc,
} = require('../src/functions/sendmail.js');

function mochatesting(num) {
    if (num <= 10) {
        return true;
    }
    return false;
}

describe('# all test', () => {
    ////////////////
    // TEST MOCHA //
    ////////////////

    describe('## the_mocha_default_test()', () => {
        it('should return -1 when the value is not present', () => {
            assert.equal([1, 2, 3].indexOf(4), -1);
        });
    });
    describe('## mochatesting()', () => {
        it('test mochatesting with params 0', () => {
            assert.equal(mochatesting(0), true);
        });
        it('test mochatesting with params 10', () => {
            assert.equal(mochatesting(10), true);
        });
        it('test mochatesting with params 11', () => {
            assert.equal(mochatesting(11), false);
        });
    });

    ///////////////////
    // TEST SENDMAIL //
    ///////////////////

    describe('## mySendMailFunc()', () => {
        it('test mySendMailFunc with no params', () => {
            assert.notEqual(mySendMailFunc(), true);
        });
    });

    //////////////////////
    // TEST USER CREATE //
    //////////////////////

    function testCreateUserIfExist(userName = `mocha_${tagOfTest}`) {
        const options = {
            url: `${fetchUrl}/users`,
            json: true,
            body: {
                userName,
                mail: 'adrien@yupanki.fr',
                pass,
                comment: '',
            },
        };
        return new Promise((resolve/* , reject */) => {
            request.post(options, (err, res/* , body */) => {
                resolve(res.statusCode);
            });
        });
    }

    describe('## userCreate()', () => {
        it('test /user (usercreate) with new name', async (/* done */) => {
            assert.equal(await testCreateUserIfExist(), 200);
        });
        it('test /user (usercreate) with existed name', async (/* done */) => {
            assert.equal(await testCreateUserIfExist(), 401);
        });
    });

    ///////////////////
    // TEST USER JWT //
    ///////////////////

    function getTockenFunc(returnArg, userName = `mocha_${tagOfTest}`) {
        const options = {
            url: `${fetchUrl}/login`,
            json: true,
            body: {
                username: userName,
                password: pass,
            },
        };
        return new Promise((resolve/* , reject */) => {
            request.post(options, (err, res/* , body */) => {
                if (returnArg === 'theStatus') {
                    resolve(res.statusCode);
                } if (returnArg === 'returnTocken') {
                    resolve(res.body.access_token);
                } if (returnArg === 'returnBody') {
                    resolve(res.body);
                }
            });
        });
    }

    function jwsRefresh(tocken) {
        const options = {
            url: `${fetchUrl}/refresh`,
            headers: {
                Authorization: `Bearer ${tocken}`,
            },
        };
        return new Promise((resolve/* , reject */) => {
            request.get(options, (err, res/* , body */) => {
                resolve(res.statusCode);
            });
        });
    }

    describe('## userJwt()', () => {
        it('POST jws login', async (/* done */) => {
            assert.equal(await getTockenFunc('theStatus'), 200);
        });
        it('GET jws refresh', async () => {
            assert.equal(await jwsRefresh(await getTockenFunc('returnTocken')), 200);
        });
    });

    //////////////////////
    // TEST USER delete //
    //////////////////////

    function deleteAccount(bodyArg) {
        const options = {
            url: `${fetchUrl}/users/${bodyArg.user_id}`,
            headers: {
                Authorization: `Bearer ${bodyArg.access_token}`,
            },
        };
        return new Promise((resolve/* , reject */) => {
            request.delete(options, (err, res/* , body */) => {
                resolve(res.statusCode);
            });
        });
    }

    describe('## user delete', () => {
        it('DELETE ok', async (/* done */) => {
            assert.equal(await deleteAccount(await getTockenFunc('returnBody')), 200);
        });
    });
});
