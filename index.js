// TIME ZONE
process.env.TZ = 'Europe/Amsterdam';
const today = new Date();
console.info('the node date: ', today.toString());
// DEPENDENCIES
require('dotenv').config();
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const app = express();
app.use(cors());
app.use(express.json());

// USE PORT
const port = 8080;

// CALL MODULES
const {
    helloWorld,
} = require('./src/functions/rootUser.js');

const {
    jwtSearchUser,
    SECRET,
    checkTokenMiddleware,
    extractBearerToken,
} = require('./src/functions/tocken.js');

/* const {
    mySendMailFunc,
} = require('./src/functions/sendmail.js'); */

// CONNECT BDD WITH MONGOOSE
mongoose.connect(process.env.DATABASE_URL, {
    authSource: process.env.DATABASE_authSource,
    user: process.env.DATABASE_user,
    pass: process.env.DATABASE_pass,
});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'error connect mongoose'));
db.once('open', async () => {
    console.log('good conexion to mongoose');
});

// RUN SERVER
app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
});

/////////////////
// THE ROOTERS //
/////////////////

// TEST RUN ROOT
app.get('/', (req, res) => {
    res.send(helloWorld);
});

// USER CREATE
/* app.post('/users', async (req, res) => {
    const rb = req.body;
    userCreate(rb.userName, rb.mail, rb.pass, rb.comment)
        .then((json) => res.send(json))
        .catch((error) => res.status(401).json({ message: error }));
}); */

// ROOT TO TEST LOGIN

app.post('/login', async (req, res) => {
    // Pas d'information à traiter
    if (!req.body.username || !req.body.password) {
        return res.status(400).json({ message: 'Error. Please enter the correct username and password' });
    }

    const users = await jwtSearchUser(req.body.username, req.body.password);
    console.log(`users in index: ${users}`);

    if (users.tocken !== true) {
        return res.status(400).json({ message: 'Error. Wrong login or password' });
    }

    const token = jwt.sign({
        id: users.id,
        username: users.username,
    }, SECRET, { expiresIn: '3 hours' });

    return res.json({ access_token: token, user_id: users.id });
});

// verifie seulement la validitée du tocken
app.get('/jwtTest/check', checkTokenMiddleware, (req, res) => {
    // Récupération du token
    const token = req.headers.authorization && extractBearerToken(req.headers.authorization);
    // Décodage du token
    const decoded = jwt.decode(token, { complete: false });
    // create new tocken
    return res.json({
        content: decoded,
    });
});

// refresh tocken
app.get('/refresh', checkTokenMiddleware, (req, res) => {
    // create new tocken
    const users = [];
    // extract tocken
    const reqtoken = req.headers.authorization && extractBearerToken(req.headers.authorization);
    const decoded = jwt.decode(reqtoken, { complete: false });
    users.id = decoded.id;
    users.username = decoded.username;
    const token = jwt.sign({
        id: users.id,
        username: users.username,
    }, SECRET, { expiresIn: '3 hours' });

    return res.json({
        newJwt: token,
    });
});

const router = express.Router();
app.use('/users', router);
require('./src/controlers/users')(app, router);
