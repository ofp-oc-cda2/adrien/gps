// DEPENDENCIES
require('dotenv').config();
const express = require('express');
const cors = require('cors');
const jwt = require('jsonwebtoken');

const app = express();
app.use(cors());
app.use(express.json());

// CREATE FUNC objectId = string
const ObjectID = require('mongodb').ObjectId;

// CALL MODULES
const {
    userCreate,
    userDelete,
    userUpdate,
    userReadOne,
    userUpdatePass,
} = require('../functions/rootUser.js');
const {
    champsAdd,
    champsGet,
    champsDelete,
    champsUpdate,
} = require('../functions/rootChamp.js');
const {
    calendarAdd,
    calendarGet,
    calendarDelete,
    calendarUpdate,
} = require('../functions/rootCalendar.js');

const {
    checkTokenMiddleware,
    extractBearerToken,
} = require('../functions/tocken.js');

module.exports = (router) => {
    router.post('/users', async (req, res) => {
        const rb = req.body;
        userCreate(req, res, rb.userName, rb.mail, rb.pass, rb.comment);
    });

    // USER DELETE
    router.delete('/users/:id', checkTokenMiddleware, async (req, res) => {
        const userId = ObjectID(req.params.id);
        if (req.connectedUser == userId) {
            userDelete(userId);
            return res.send('delete is ok');
        }
        return res.status(401).json({ message: 'Error. Bad token' });
    });

    // USER UPDATE
    router.put('/users/:id', checkTokenMiddleware, async (req, res) => {
        const userId = ObjectID(req.params.id);
        const {
            userName,
            mail,
            comment,
        } = req.body;
        if (
            userName === ''
            || userName === undefined
            || userName === null
            || mail === ''
            || mail === undefined
            || mail === null
        ) {
            return res.status(401).json({ message: 'Error. empty string' });
        }
        if (req.connectedUser == userId) {
            userUpdate(userId, userName, mail, comment);
            return res.status(200).json({ message: 'update tocken is ok' });
        }
        return res.status(401).json({ message: 'Error. Bad token' });
    });

    // USER UPDATE PASSWORD
    router.put('/users/:id/pass', checkTokenMiddleware, async (req, res) => {
        const userId = ObjectID(req.params.id);
        const {
            oldPass,
            newPass,
        } = req.body;
        if (
            oldPass === ''
            || oldPass === undefined
            || oldPass === null
            || newPass === ''
            || newPass === undefined
            || newPass === null
        ) {
            return res.status(401).json({ message: 'Error. empty string' });
        }
        if (req.connectedUser == userId) {
            userUpdatePass(userId, res, oldPass, newPass);
        } else {
            return res.status(401).json({ message: 'Error. Bad token' });
        }
        return 'end';
    });
    // USER READ ONE
    router.get('/users/:id', checkTokenMiddleware, async (req, res) => {
        // Récupération du token
        const token = req.headers.authorization && extractBearerToken(req.headers.authorization);
        // Décodage du token
        const decoded = jwt.decode(token, { complete: false });
        const userId = ObjectID(req.params.id);
        if (decoded.id == userId) {
            userReadOne(userId, res, true);
            /* const toReturn = await userReadOne(userId, res, true);
            return res.send(toReturn); */
        }
    });
    // USER READ ONE GRAPHIC
    router.post('/users/:id/graphic', checkTokenMiddleware, async (req, res) => {
        const userId = ObjectID(req.params.id);
        if (req.connectedUser == userId) {
            userReadOne(userId, res, false, 'graphic', req.body.periods, req.body.numberOfPeriods);
            /* const toReturn = await userReadOne(userId, res, true);
            return res.send(toReturn); */
        }
    });

    // USER > CHAMP SINGLE > ADD
    router.post('/users/:id/champs/', checkTokenMiddleware, async (req, res) => {
        // Récupération du token
        const token = req.headers.authorization && extractBearerToken(req.headers.authorization);
        // Décodage du token
        const decoded = jwt.decode(token, { complete: false });
        const userId = ObjectID(req.params.id);
        const {
            fieldName,
            fieldColor,
            fieldComment,
        } = req.body;
        if (decoded.id == userId) {
            champsAdd(res, userId, fieldName, fieldColor, fieldComment);
        }
    });

    // USER > CHAMP SINGLE > READ
    router.get('/users/:id/champs/:fieldId', checkTokenMiddleware, async (req, res) => {
        // Récupération du token
        const token = req.headers.authorization && extractBearerToken(req.headers.authorization);
        // Décodage du token
        const decoded = jwt.decode(token, { complete: false });

        const userId = ObjectID(req.params.id);
        const fieldId = ObjectID(req.params.fieldId);
        if (decoded.id == userId) {
            champsGet(res, userId, fieldId);
        }
    });

    // USER > CHAMP SINGLE > REMOVE
    router.delete('/users/:id/champs/:fieldId', checkTokenMiddleware, async (req, res) => {
        // Récupération du token
        const token = req.headers.authorization && extractBearerToken(req.headers.authorization);
        // Décodage du token
        const decoded = jwt.decode(token, { complete: false });

        const userId = ObjectID(req.params.id);
        const fieldId = ObjectID(req.params.fieldId);
        if (decoded.id == userId) {
            champsDelete(res, userId, fieldId);
        }
    });

    // USER > CHAMP SINGLE > UPDATE
    router.put('/users/:id/champs/:fieldId', checkTokenMiddleware, async (req, res) => {
        // Récupération du token
        const token = req.headers.authorization && extractBearerToken(req.headers.authorization);
        // Décodage du token
        const decoded = jwt.decode(token, { complete: false });

        const userId = ObjectID(req.params.id);
        const fieldId = ObjectID(req.params.fieldId);
        const { content } = req.body;
        content._id = fieldId;
        if (decoded.id == userId) {
            champsUpdate(res, userId, fieldId, content);
        }
    });

    // USER > CHAMP SINGLE > CALENDAR > ADD
    router.post('/users/:id/champs/:fieldId', checkTokenMiddleware, async (req, res) => {
        // Récupération du token
        const token = req.headers.authorization && extractBearerToken(req.headers.authorization);
        // Décodage du token
        const decoded = jwt.decode(token, { complete: false });

        const userId = ObjectID(req.params.id);
        const fieldId = ObjectID(req.params.fieldId);
        const calendarDate = req.body.date;
        console.log('calendarDate');
        console.log(calendarDate);
        const calendarValue = req.body.value;
        console.log('calendarValue');
        console.log(calendarValue);
        if (decoded.id == userId) {
            calendarAdd(res, userId, fieldId, calendarDate, calendarValue);
        }
    });

    // USER > CHAMP SINGLE > CALENDAR > READ (FOR CHAMP)
    router.get('/users/:id/champs/:fieldId/calendar', checkTokenMiddleware, async (req, res) => {
        // Récupération du token
        const token = req.headers.authorization && extractBearerToken(req.headers.authorization);
        // Décodage du token
        const decoded = jwt.decode(token, { complete: false });

        const userId = ObjectID(req.params.id);
        const fieldId = ObjectID(req.params.fieldId);
        if (decoded.id == userId) {
            calendarGet(res, userId, fieldId);
        }
    });

    // USER > CHAMP SINGLE > CALENDAR > DELETE
    router.delete('/users/:id/champs/:fieldId/calendar/:calendarId', checkTokenMiddleware, async (req, res) => {
        // Récupération du token
        const token = req.headers.authorization && extractBearerToken(req.headers.authorization);
        // Décodage du token
        const decoded = jwt.decode(token, { complete: false });

        const userId = ObjectID(req.params.id);
        const fieldId = ObjectID(req.params.fieldId);
        const calendarId = ObjectID(req.params.calendarId);
        if (decoded.id == userId) {
            calendarDelete(res, userId, fieldId, calendarId);
        }
    });

    // USER > CHAMP SINGLE > CALENDAR > UPDATE
    router.put('/users/:id/champs/:fieldId/calendar/:calendarId', checkTokenMiddleware, async (req, res) => {
        // Récupération du token
        const token = req.headers.authorization && extractBearerToken(req.headers.authorization);
        // Décodage du token
        const decoded = jwt.decode(token, { complete: false });

        const userId = ObjectID(req.params.id);
        const fieldId = ObjectID(req.params.fieldId);
        const calendarId = ObjectID(req.params.calendarId);
        const { value, date } = req.body;
        if (decoded.id == userId) {
            calendarUpdate(res, userId, fieldId, calendarId, value, date);
        }
    });
};
