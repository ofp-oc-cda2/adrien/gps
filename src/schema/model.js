const mongoose = require('mongoose');

// SCHEMA > USER
const user = new mongoose.Schema({
    userName: String,
    mail: String,
    hash: String,
    comment: String,
    champs: [
        {
            name: String,
            color: String,
            comment: String,
        },
    ],
});
const User = mongoose.model('User', user);

// SCHEMA > CALENDAR
const calendar = new mongoose.Schema({
    date: Date,
    value: Number,
    userId: String, // clef étrangère correspondant au user
    fieldId: String, // clef étrangère correspondant au champ dans le user
});
const Calendar = mongoose.model('Calendar', calendar);

// EXPORT MODULES
module.exports = {
    User,
    Calendar,
};
