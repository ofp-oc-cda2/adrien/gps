// CALL MODULES
const bcrypt = require('bcrypt');
const { User, Calendar } = require('../schema/model.js');

const {
    mySendMailFunc,
} = require('./sendmail.js');
const {
    userReadOneGraphic,
} = require('./graphic');

// HELLO WORLD
const helloWorld = `<HTML>
<style>
    body{
        background-color:black;
        height:100vh;
        margin:0;
        display:flex;
        flex-direction:column;
        align-items:center;
        justify-content:center;
        font-family:sans;
        color:green;
    }
    h1{
        font-size:3rem;
        text-align:center;
    }
</style>
<body>
    <h1>Hello World Again!<br>It's a Gnu Personal Stats app V.1.0.0</h1>
    <p>work in progress...Please wait</p>
</body>
</HTML>`;

///////////////
// FUNCTIONS //
///////////////

// USER CREATE
async function userCreate(req, res, userName, mail, pass, comment) {
    if (userName === '' || mail === '' || pass === '') {
        return res.send('un champ est vide!');
    }
    const ifExist = await User.find({ userName });
    console.log(ifExist);
    if (ifExist.length != 0) {
        return res.status(401).json({ message: 'un user porte le meme nom!' });
    }
    const salt = await bcrypt.genSalt(12);
    const hashing = await bcrypt.hash(pass, salt);
    await User.create({
        userName,
        mail,
        hash: hashing,
        comment,
    });
    const message = `Vous avez créer le compte ${userName}`;
    mySendMailFunc(message, mail);
    return res.send({
        userName,
        mail,
        hash: hashing,
        comment,
    });
}

// USER DELETE
async function userDelete(userId) {
    await Calendar.deleteMany({ userId });
    await User.findByIdAndDelete(userId);
}

// USER UPDATE
async function userUpdate(userId, userName, mail, comment) {
    await User.findOneAndUpdate({ _id: userId }, { userName, mail, comment });
}

// USER UPDATE PASS
async function userUpdatePass(userId, res, oldPass, newPass) {
    const hash = await User.findById(userId, 'hash');
    // compare hash et password old
    const isValidOldPass = await bcrypt.compareSync(oldPass, hash.hash);
    // if compare is ok, create new hash and modify
    if (isValidOldPass === true) {
        const salt = await bcrypt.genSalt(12);
        const hashing = await bcrypt.hash(newPass, salt);
        await User.updateOne({ _id: userId }, {
            hash: hashing,
        });
        return res.status(200).json({ message: 'all right' });
    }
    return res.status(401).json({ message: 'old password is bad' });
}

// USER READ ONE
async function userReadOne(userId, res, allFields, finality = '', periods = '', numberOfPeriods = 0) {
    if (allFields === true) {
        const user = await User.findById(userId, '_id userName mail comment champs.name champs.color champs.comment champs._id');
        // return res.send(user);
        if (finality === '') {
            return res.send(user);
        } if (finality === 'graphic') {
            userReadOneGraphic(res, user);
            return 'end of userReadOne func';
        }
    } else if (allFields === false) {
        const user = await User.findById(userId, '_id userName mail comment champs.name champs.color champs.comment champs._id');
        const calendars = await Calendar.find({ userId }); // renvoi les calendars du user
        const allDefFields = user.champs.map((field) => ({
            ...field.toJSON(),
            calendars: calendars
                .filter((cal) => cal.fieldId === field._id.toString())
                .map((cal) => ({
                    ...cal.toJSON(),
                    /* __v: undefined, // pour suppr une clef */
                })),
        }));

        // OUTPOUT KITCHEN
        const outpout = {
            ...user.toJSON(),
            champs: allDefFields,
        };

        if (finality === '') {
            console.log('in if standard');
            return res.send(outpout);
        } if (finality === 'graphic') {
            userReadOneGraphic(res, outpout, periods, numberOfPeriods);
            return 'end of userReadOne func';
        }
    }
    return res.send('opdate tocken required!');
}

// EXPORT MODULES
module.exports = {
    helloWorld,
    userCreate,
    userDelete,
    userUpdate,
    userUpdatePass,
    userReadOne,
};
