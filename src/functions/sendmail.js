//const resp = require('express/lib/response');
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');

function mySendMailFunc(message, mail) {
    let securemail = true;
    if (process.env.MAIL_SECURE === 'true') {
        securemail = true;
    } else if (process.env.MAIL_SECURE === 'false') {
        securemail = false;
    }
    const transporter = nodemailer.createTransport(smtpTransport({
        host: process.env.MAIL_HOST, //mail.example.com (your server smtp)
        port: parseInt(process.env.MAIL_PORT, 10), //2525 (specific port)
        secureConnection: securemail, //true or false
        auth: {
            user: process.env.MAIL_USER,
            pass: process.env.MAIL_PASS,
        },
    }));

    const mailOptions = {
        from: 'adrien@yupanki.fr',
        to: mail,
        subject: 'test create account',
        text: message,
    };

    /* transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log('in mySendMailFunc', error);
            //console.log(typeof process.env.MAIL_SECURE);
        } else {
            console.log(`Email sent: ${info.response}`);
            //console.log(typeof process.env.MAIL_SECURE);
            //return res.send('mail envoyer');
        }
    }); */

    transporter.sendMail(mailOptions);
}

// EXPORT MODULES
module.exports = {
    mySendMailFunc,
};
