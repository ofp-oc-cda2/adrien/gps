function fieldCalToListValue(element, thisPeriod, index, toReturn, periods) {
    const totoReturn = toReturn;
    // console.log(totoReturn);
    // console.log(`fieldCalToListValue - periods: ${thisPeriod} - element: `, element);
    // console.log(`element.name : ${element.name}`);
    // add name: value

    let valuesOfPeriods = 0;
    for (let index2 = 0; index2 < element.calendars.length; index2 += 1) {
        console.log('element.calendars[index2] : ', element.calendars[index2]);
        const forDate = element.calendars[index2].date;
        const forPeriodYear = forDate.getFullYear();
        const forPeriodmonth = forDate.getMonth() + 1;
        let forPeriod = '';
        if (periods === 'year') {
            forPeriod = `${forPeriodYear}`;
        } else if (periods === 'month') {
            forPeriod = `${forPeriodYear}-${forPeriodmonth}`;
        }
        if (forPeriod === thisPeriod) {
            valuesOfPeriods += element.calendars[index2].value;
        }
    }

    // console.log('element.calendars : ', element.calendars);
    totoReturn[index][`${element.name}`] = valuesOfPeriods;
    return totoReturn;
}

function userReadOneGraphic(res, userObj, periods, numberOfPeriods) {
    // userObj = user + fields + calendars
    console.log('numberOfPeriods : ', numberOfPeriods);
    // construction label pattern
    const dateRound = new Date();
    let thisPeriodYear = dateRound.getFullYear();
    let thisPeriod = '';
    let thisPeriodmonth = dateRound.getMonth() + 1;
    if (periods === 'week') {
        console.log('if week');
        const oneJan = new Date(dateRound.getFullYear(), 0, 1);
        const numberOfDays = Math.floor((dateRound - oneJan) / (24 * 60 * 60 * 1000));
        const result = Math.ceil((dateRound.getDay() + 1 + numberOfDays) / 7);
        thisPeriod = `${thisPeriodYear}_s${result}`;
        console.log(`thisPeriod : ${thisPeriod}`);
    } else if (periods === 'month') {
        console.log('if month');
        thisPeriod = `${thisPeriodYear}-${thisPeriodmonth}`;
        console.log(`thisPeriod : ${thisPeriod}`);
    } else if (periods === 'year') {
        console.log('if year');
        thisPeriod = `${thisPeriodYear}`;
        console.log(`thisPeriod : ${thisPeriod}`);
    } else {
        console.log('error in type of periods');
        return res.status(400).send();
    }
    // créer les étiquettes [{name: period}, {name: period}]
    let toReturn = [];

    for (let index = 0; index < numberOfPeriods; index += 1) {
        // push dans toReturn des {name: nomDeLaPériode}
        const forPush = { name: thisPeriod };
        toReturn.push(forPush);
        // boucle sur chaque field
        for (let index2 = 0; index2 < userObj.champs.length; index2 += 1) {
            const element = userObj.champs[index2];
            // this function convert champ with calendar to array value of periods
            toReturn = fieldCalToListValue(element, thisPeriod, index, toReturn, periods);
        }
        // décrémente la période
        if (periods === 'month') {
            if (thisPeriodmonth > 1) {
                thisPeriodmonth -= 1;
            } else {
                thisPeriodmonth = 12;
                thisPeriodYear -= 1;
            }
            thisPeriod = `${thisPeriodYear}-${thisPeriodmonth}`;
        } else if (periods === 'year') {
            thisPeriodYear -= 1;
            thisPeriod = `${thisPeriodYear}`;
        } else if (periods === 'week') {
            //
        }
    }

    console.log('toReturn : ', toReturn);

    // func return periods
    return res.send(toReturn);
}
module.exports = {
    userReadOneGraphic,
};
