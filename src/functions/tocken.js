// CALL MODULES
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {
    User,
    Calendar,
} = require('../schema/model.js');

// A VENIR: FUNCTIONS D'AUTHENTIFICATIONS

// VERIF TOCKEN USER
async function userTocken(userId, pass) {
    // console.log('pass: '+pass);
    // console.log('id: '+userId);
    const user = await User.findOne({ _id: userId });
    const { hash } = user;
    const isValidPass = bcrypt.compareSync(pass, hash);
    //const isValidPass = bcrypt.compareSync('0000', hash);
    return isValidPass;
}

// VERIF CHAMP EXIST FOR CALENDAR => return true/false if fieldId exist in userId
async function champExist(userId, fieldId) {
    try {
        const user = await User.findById(userId, '_id champs._id');
        const filterUser = user.champs.find(
            (champs) => {
                console.log(`toto3 ${champs}`);
                return champs._id.toString() === fieldId.toString();
            },
        );
        //console.log(filterUser);
        return filterUser !== undefined;
    } catch {
        return false;
    }
}

// verif si new calendar n'est pas sur user.champ.day existant
async function tockenIsExistDay(userId, fieldId, calendarDate) {
    const searchCalendar = await Calendar.find({ userId, fieldId, date: calendarDate });
    console.log('searchCalendar');
    console.log(searchCalendar);
    if (searchCalendar.length === 0) {
        return true;
    }
    return false;
}

async function jwtSearchUser(userName, userPass) {
    console.log(userName);
    const searchUserForJwt = await User.findOne({ userName });
    // console.log(`count: ${searchUserForJwt.length}`);
    if (searchUserForJwt === null) {
        return false;
    }/*
    if (searchUserForJwt.length === 0) {
        return false;
    } */
    //console.log(`searchUserForJwt: ${searchUserForJwt}`);
    const { _id } = searchUserForJwt;
    const jwtUserTocken = await userTocken(_id, userPass);
    //console.log(`userPass: ${userPass}`);
    //console.log(`userId: ${_id}`);
    //console.log(`userTocken: ${userTocken}`);
    const verifPass = {
        tocken: jwtUserTocken,
        id: _id,
        username: userName,
    };
    //console.log(`const verifPass in tocken.js: ${verifPass}`);
    return verifPass;
}

const SECRET = 'mykey';

/* Récupération du header bearer */
const extractBearerToken = (headerValue) => {
    if (typeof headerValue !== 'string') {
        return false;
    }

    const matches = headerValue.match(/(bearer)\s+(\S+)/i);
    return matches && matches[2];
};

/* Vérification du token */
const checkTokenMiddleware = (req, res, next) => {
    // Récupération du token
    const token = req.headers.authorization && extractBearerToken(req.headers.authorization);
    // console.log(token);
    // Présence d'un token
    if (!token) {
        return res.status(401).json({ message: 'Error. Need a token' });
    }

    function veryExtractTocken() {
        // Véracité du token
        jwt.verify(token, SECRET, (err) => {
            if (err) {
                return res.status(401).json({ message: 'Error. Bad token' });
            }
            // Décodage du token
            const decoded = jwt.decode(token, { complete: false });
            req.connectedUser = decoded.id;
            //return res.json({ content: decoded });

            return next();
        });
    }

    veryExtractTocken();
};

// EXPORT MODULES
module.exports = {
    userTocken,
    champExist,
    tockenIsExistDay,
    jwtSearchUser,
    SECRET,
    checkTokenMiddleware,
    extractBearerToken,
};
