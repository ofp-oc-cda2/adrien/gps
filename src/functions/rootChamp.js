// CALL MODULES
//const res = require("express/lib/response");
//const bcrypt = require('bcrypt');
const { User, Calendar } = require('../schema/model.js');
/* const {
    userTocken,
} = require('./tocken.js'); */
const {
    userReadOne,
} = require('./rootUser.js');

///////////////
// FUNCTIONS //
///////////////

function filterFixBug(fieldName) {
    // forbiden to name field 'name'
    if (fieldName === 'name') {
        return false;
    }
    return true;
}

// CHAMP ADD
async function champsAdd(res, userId, fieldName, fieldColor, fieldComment) {
    if (filterFixBug(fieldName) === false) {
        return res.status(400).json({ message: 'Error. field name is incorrect' });
    }
    async function insertChamps() {
        await User.findOneAndUpdate(
            { _id: userId },
            {
                $push: {
                    champs: {
                        name: fieldName,
                        color: fieldColor,
                        comment: fieldComment,
                    },
                },
            },
        );
        const userReadChamps = await User.findOne({ _id: userId }, 'champs._id champs.name champs.color champs.comment');
        const UserReturnNewField = userReadChamps.champs[userReadChamps.champs.length - 1];
        return res.send(UserReturnNewField);
    }
    insertChamps(userId, fieldName, fieldColor, fieldComment);
}

// CHAMP GET (one champs view)
async function champsGet(res, userId, fieldId) {
    const user = await userReadOne(userId, res, true);
    const filterUser = user.champs.find(
        (champs) => champs._id.toString() === fieldId.toString(),
    );
    console.log('point');
    return res.send(filterUser);
}

// CHAMP DELETE
async function champsDelete(res, userId, fieldId) {
    // const user = await userReadOne(userId, res, true);
    const user = await User.findById(userId, '_id userName mail comment champs.name champs.color champs.comment champs._id');
    console.log('user');
    console.log(user);
    const userarray = user.champs;
    const deleteIndex = userarray.findIndex(
        (champs) => champs._id.toString() === fieldId.toString(),
    );
    userarray.splice(deleteIndex, 1); // suppr dans const userarray
    await Calendar.deleteMany({ userId, fieldId });
    await User.findOneAndUpdate({ _id: userId }, { champs: userarray });
    return res.send(userarray); // error if deleteIndex only
}

// CHAMP UPDATE
async function champsUpdate(res, userId, fieldId, content) {
    if (filterFixBug(content.name) === false) {
        return res.status(400).json({ message: 'Error. field name is incorrect' });
    }
    // const user = await userReadOne(userId, res, true);
    const user = await User.findById(userId, '_id userName mail comment champs.name champs.color champs.comment champs._id');
    const userarray = user.champs;
    const champsIndex = userarray.findIndex(
        (champs) => champs._id.toString() === fieldId.toString(),
    );
    //const theIdObject = userarray[champsIndex]['_id'];
    // suppr dans const userarray
    userarray.splice(champsIndex, 1);
    // ajoute mais modifie _id, voir aussi si index inchangé
    userarray.splice(champsIndex, 0, content);
    await User.findOneAndUpdate({ _id: userId }, { champs: userarray });
    return res.send(userarray);
}

// EXPORT MODULES
module.exports = {
    champsAdd,
    champsGet,
    champsDelete,
    champsUpdate,
};
