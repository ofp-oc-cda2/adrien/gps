// CALL MODULES
//const res = require("express/lib/response");
//const bcrypt = require('bcrypt');

const {
    /* User, */
    Calendar,
} = require('../schema/model.js');

const {
    champExist,
    tockenIsExistDay,
} = require('./tocken.js');

///////////////
// FUNCTIONS //
///////////////

function secureValueIsNum(value) {
    // interdire injection value NaN
    if (typeof value === 'string') {
        // eslint-disable-next-line use-isnan
        if (Number.isNaN(parseFloat(value))) {
            return false;
        }
        return true;
    } if (typeof value === 'number') {
        return true;
    }
    return false;
}

// CALENDAR ADD
async function calendarAdd(res, userId, fieldId, calendarDate, calendarValue) {
    if (secureValueIsNum(calendarValue) === false) {
        return res.status(401).json({ message: 'Error. not a number' });
    }
    // verifier existance fieldId dans userId
    const tockenFieldExist = await champExist(userId, fieldId);
    if (tockenFieldExist === true) {
        // verifier qu'il n'existe pas une entrer pour le meme jour sur le même champ
        const tockenExistDay = await tockenIsExistDay(userId, fieldId, calendarDate);
        //console.log(`return of tockenExistDay: ${tockenExistDay}`);
        if (tockenExistDay === true) {
            await Calendar.create([{
                date: calendarDate,
                value: calendarValue,
                userId,
                fieldId,
            }]);
            const calAddReturn = await Calendar.findOne({
                date: calendarDate,
                value: calendarValue,
                userId,
                fieldId,
            });
            return res.send(calAddReturn);
        }
        return res.status(422).send('il existe déjà une entrer pour ce jour dans ce champ');
    }
    return res.status(404).send('the field of new calendar does not exist');
}

// CALENDAR READ FOR CHAMP
async function calendarGet(res, userId, fieldId) {
    const tockenFieldExist = await champExist(userId, fieldId);
    if (tockenFieldExist === true) {
        // recup les entrés de calendar pour le champ
        const resultFind = await Calendar.find({ userId, fieldId });
        //console.log(resultFind);
        return res.send(resultFind);
    }
    return res.status(404).send('the field does not exist');
}

//CALENDAR DELETE
async function calendarDelete(res, userId, fieldId, calendarId) {
    // ici: verif if exist? avec les 3 keys
    const resultFindOne = await (await Calendar.find({
        userId,
        fieldId,
        _id: calendarId,
    })).length;
    //si exist => suppr
    if (resultFindOne === 1) {
        await Calendar.findOneAndDelete({
            userId,
            fieldId,
            _id: calendarId,
        });
        return res.send({
            userId,
            fieldId,
            calendarId,
        });
    }
    return res.status(404).send('the calendar does not exist');
}

//CALENDAR UPDATE
async function calendarUpdate(res, userId, fieldId, calendarId, value, date) {
    if (secureValueIsNum(value) === false) {
        return res.status(401).json({ message: 'Error. not a number' });
    }
    // ici: verif if exist? avec les 3 keys
    const resultFindOne = await (await Calendar.find({
        userId,
        fieldId,
        _id: calendarId,
    })).length;
    //si exist => suppr
    if (resultFindOne === 1) {
        await Calendar.findOneAndUpdate({
            userId,
            fieldId,
            _id: calendarId,
        }, { value, date });

        const resultFindOneAfter = await (await Calendar.find({
            userId,
            fieldId,
            _id: calendarId,
        }));
        return res.send(resultFindOneAfter);
    }
    return res.status(404).send('the calendar does not exist');
}
// EXPORT MODULES
module.exports = {
    calendarAdd,
    calendarGet,
    calendarDelete,
    calendarUpdate,
};
